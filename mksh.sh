#!/bin/sh

. "$VERCMPROOT/format/default.sh" 

comparever_mksh() {
    # mksh's versioning scheme is "R<version><optional patch letter>"
    # e.g., R56a is the first patch release following R56.
    # this means 'sort' sorts mksh's versions correctly.
    [ "$1" != "$2" ] || { echo '=' && return 0; }

    [ "$(printf "$1\\n$2" | sort)" = "$(printf "$1\\n$2")" ] \
    && echo '<' \
    || echo '>'
}
comparever() { comparever_mksh $@; }
