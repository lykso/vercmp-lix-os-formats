#!/bin/sh

. "$VERCMPROOT/format/default.sh" 

comparever_default_after_dash() {
    comparever_default "$(echo "$1" | cut -d- -f2)" "$(echo "$2" | cut -d- -f2)"
}
comparever() { comparever_default_after_dash $@; }
