#!/bin/sh

set -e

comparever() { comparever_src_git_type "$@"; }

# accepts two git commit hashes belonging to a git repository managed by the
# `src` utility.  assumes both are on the same branch.
comparever_src_git_type() {
    v1="$1"
    v2="$2"
    $SRCROOT="$(dirname $(which src))"

    # equal?
    [ "$v1" != "$v2" ] || { echo "==" && return 0; }

    cd $SRCROOT/pkg/$fmt/git

    # assume git version >= 1.8.0
    git merge-base --is-ancestor $1 $2 \
    && echo "<" \
    || echo ">"
}
