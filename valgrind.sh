#!/bin/sh

. "$VERCMPROOT/format/default.sh" 

comparever_valgrind() {
    comparever_default "$(echo "$1" | sed 's/\.\(RC[0-9]\+\)$/-\1/i')" \
                       "$(echo "$2" | sed 's/\.\(RC[0-9]\+\)$/-\1/i')"
}
comparever() { comparever_valgrind $@; }
